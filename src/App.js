import React, { useState, Fragment, useRef } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { CheckIcon } from "@heroicons/react/24/outline";
import { XCircleIcon } from "@heroicons/react/20/solid";

function App() {
  const functionURL =
    "https://of3ylvuu3ae4vgllicrhxrm5ca0tnkif.lambda-url.us-east-1.on.aws/";
  const [companyWebsite, setCompanyWebsite] = useState("");

  const [open, setOpen] = useState(false);
  const [jsonContent, setJsonContent] = useState({contains: []});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  const [gatherStyles, setGatherStyles] = useState(false);
  const gatherStylesOptions = [
    { value: true, title: "Yes" },
    { value: false, title: "No" },
  ];

  const [excludeTags, setExcludeTags] = useState(true);
  const excludeTagsOptions = [
    { value: true, title: "Yes" },
    { value: false, title: "No" },
  ];

  const [attributesToKeep, setAttributesToKeep] = useState("");

  const cancelButtonRef = useRef(null);

  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log("Sending request...");
    setLoading(true);
    const bodyParams = {
      url: `https://${companyWebsite}`,
      content: "",
      gatherStyles,
      excludeTags,
      attributesToKeep: attributesToKeep.split(",").map((attr) => attr.trim()),
    };
    
    try {
      const jsonResponse = await fetch(functionURL, {
        method: "POST",
        body: JSON.stringify(bodyParams),
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (!jsonResponse.ok) {
        const json = await jsonResponse.json();
        throw new Error(json.error);
      }
      const json = await jsonResponse.json();
      setJsonContent(json);
      setLoading(false);
      setOpen(true);
    } catch (err) {
      console.error(err);
      setLoading(false);
      setError(err.message);
  }
  };

  return (
    <div className="items-center justify-center min-h-screen text-center flex">
      <div className="w-1/3">
        {error && (
          <div className="rounded-md bg-red-50 p-4 mb-5">
          <div className="flex">
            <div className="flex-shrink-0">
              <XCircleIcon
                className="h-5 w-5 text-red-400"
                aria-hidden="true"
              />
            </div>
            <div className="ml-3">
              <h3 className="text-sm font-medium text-red-800">
                There were an error with your submission
              </h3>
              <div className="mt-2 text-sm text-red-700">
                Oh nooooo! Something went wrong. Please try again. <br />
                {error}
              </div>
              <button 
                className="rounded-md font-medium bg-red-50 hover:bg-red-100 px-2 py-1.5 focus:outline-none focus:ring-2 focus:ring-red-800 focus:ring-offset-2 focus:ring-offset-red-50 mt-2 text-sm text-red-700"
                onClick={() => setError(false)}>
                Dismiss
              </button>
            </div>
          </div>
        </div>
          )}
        <form onSubmit={handleSubmit}>
          <label
            htmlFor="company-website"
            className="block text-sm font-medium leading-6 text-gray-900"
          >
            Website URL
          </label>
          <div className="mt-2 flex flex-col items-center">
            <div className="items-center flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
              <span className="flex select-none items-center pl-3 text-gray-500 sm:text-sm">
                https://
              </span>
              <input
                type="text"
                value={companyWebsite}
                onChange={(e) => setCompanyWebsite(e.target.value)}
                className="w-screen flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                placeholder="www.example.com"
              />
            </div>
            <div className="mt-8">
              <label className="text-sm font-medium text-gray-900">
                Style gathering
              </label>
              <p className="text-xs text-gray-500">
                Do you want to copy the 'style' attribute of HTML elements to the JSON result?
              </p>
              <fieldset className="mt-4">
                <legend className="sr-only">Style gathering options</legend>
                <div className="justify-around flex flex-row">
                  {gatherStylesOptions.map((option) => (
                    <div key={option.title} className="flex items-center">
                      <input
                        id={option.title}
                        name="gather-style-option"
                        type="radio"
                        checked={gatherStyles === option.value}
                        onChange={() => setGatherStyles(option.value)}
                        className="h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-600"
                      />
                      <label
                        htmlFor={option.title}
                        className="ml-3 block text-sm font-medium leading-6 text-gray-900"
                      >
                        {option.title}
                      </label>
                    </div>
                  ))}
                </div>
              </fieldset>
            </div>
            <div className="mt-8">
              <label className="text-sm font-medium text-gray-900">
                Tags exclusion
              </label>
              <p className="text-xs text-gray-500">
                Do you want to remove the 'tagName' attribute from the elements of the JSON result?
              </p>
              <fieldset className="mt-4">
                <legend className="sr-only">Tags exclusion options</legend>
                <div className="justify-around flex flex-row">
                  {excludeTagsOptions.map((option) => (
                    <div key={option.title} className="flex items-center">
                      <input
                        id={option.title}
                        name="exclude-tags-option"
                        type="radio"
                        checked={excludeTags === option.value}
                        onChange={() => setExcludeTags(option.value)}
                        className="h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-600"
                      />
                      <label
                        htmlFor={option.title}
                        className="ml-3 block text-sm font-medium leading-6 text-gray-900"
                      >
                        {option.title}
                      </label>
                    </div>
                  ))}
                </div>
              </fieldset>
            </div>
            <div className="mt-8">
              <label
                htmlFor="attributes-to-keep"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Attributes to keep
              </label>
              <p className="text-xs text-gray-500">
                Enter the HTML attributes you want to keep, separated by commas.
              </p>
              <div className="mt-2">
                <input
                  type="text"
                  name="attributes-to-keep"
                  id="attributes-to-keep"
                  value={attributesToKeep}
                  onChange={(e) => setAttributesToKeep(e.target.value)}
                  className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  placeholder="data-uwid, class, id"
                />
              </div>
            </div>
          </div>
          <div className="mt-6">
            <button
              type="submit"
              disabled={loading}
              className={`inline-flex w-full justify-center items-center rounded-md border border-transparent px-3 py-2 ${loading ? 'bg-neutral-400' : 'bg-indigo-600 hover:bg-indigo-500'} text-sm font-semibold text-white shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600`}
            >
              Submit
              {loading && (
                <svg
                  aria-hidden="true"
                  className="w-5 h-5 ml-5 text-neutral-50 animate-spin fill-indigo-600"
                  viewBox="0 0 100 101"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                    fill="currentColor"
                  />
                  <path
                    d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                    fill="currentFill"
                  />
                </svg>
              )}
            </button>
            {loading && (
              <p className="text-xs text-gray-500 mt-2">
                Please wait while we process your request... <br /> It can take up to a few minutes.
              </p>
            )}
          </div>
        </form>
      </div>
      <Transition.Root show={open} as={Fragment}>
        <Dialog
          as="div"
          className="relative z-10"
          initialFocus={cancelButtonRef}
          onClose={setOpen}
        >
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
          </Transition.Child>

          <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
            <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                enterTo="opacity-100 translate-y-0 sm:scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              >
                <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg sm:p-6">
                  <div>
                    <div className="mx-auto flex h-12 w-12 items-center justify-center rounded-full bg-green-100">
                      <CheckIcon
                        className="h-6 w-6 text-green-600"
                        aria-hidden="true"
                      />
                    </div>
                    <div className="mt-3 text-center sm:mt-5">
                      <Dialog.Title
                        as="h3"
                        className="text-base font-semibold leading-6 text-gray-900"
                      >
                        Request successful
                      </Dialog.Title>
                      <div className="mt-2">
                        <p className="text-sm text-gray-500">
                          Here is the result of your request. You can download it as a JSON file by clicking the button below.
                        </p>
                      </div>
                      <div className="mt-2">
                        <p className="text-base text-gray-500 text-left">
                          {jsonContent.contains.map((section) => (
                            section.category !== "undefined" ?
                            <div key={section.section} className="mt-4">
                              <p className="text-sm font-semibold">
                                {section.category}
                              </p>
                              <p className="text-xs text-gray-500">
                                {section.contains.map((element) => (
                                  element.object === "primitive" ?
                                  <div className="ml-5">
                                    <p className="text-xs text-gray-500">
                                      - {element.type === "text" ? element.textValue :
                                      `<${element.type}>`}
                                    </p>
                                  </div>
                                  : element.object === "meaning unit" ?
                                  <div className="ml-5">
                                    <p className="text-xs text-gray-500 font-semibold">
                                      - {`<meaning unit>`}
                                    </p>
                                    {element.contains.map((element2) => (
                                      <div className="ml-5">
                                      <p className="text-xs text-gray-500">
                                      - {element2.type === "text" ? element2.textValue :
                                      `<${element2.type}>`}
                                      </p>
                                    </div>
                                    ))}
                                  </div>
                                  : null
                                ))}
                              </p>
                            </div>
                            : null
                          ))
                          }
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="mt-5 sm:mt-6 sm:grid sm:grid-flow-row-dense sm:grid-cols-2 sm:gap-3">
                    <a
                      type="button"
                      className="inline-flex w-full justify-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 sm:col-start-2"
                      href={`data:text/json;charset=utf-8,${encodeURIComponent(
                        JSON.stringify(jsonContent),
                      )}`}
                      download={`narrative-response-${companyWebsite}.json`}
                    >
                      Download JSON
                    </a>
                    <button
                      type="button"
                      className="mt-3 inline-flex w-full justify-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 sm:col-start-1 sm:mt-0"
                      onClick={() => setOpen(false)}
                      ref={cancelButtonRef}
                    >
                      Close
                    </button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition.Root>
    </div>
  );
}

export default App;
